﻿using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using Shouldly;
using WebAutomation.Selenium;
using WebAutomation.Reporter;


namespace WebAutomaton.Tests
{
    [TestFixture]
    public class KendoMultiSelectTests : BaseSeleniumWrapperTest
    {
        [Test]
        public void Given_FiveElementsSelected_WhenCleared_Then_NoElementsSelected()
        {
            page._driver.Url =
                "file:///C:/Users/pawy/source/repos/webautomation/WebAutomation/KendoMultiSelect.html";
            page._driver.Navigate();

            ICustomWebElement<KendoMultiSelect> elementkedno =
                page.FindElement(By.Id("required")).AsCustomElement<KendoMultiSelect>();
            elementkedno.SelectByText(new string[]
                {"Anne King", "Nancy King", "Anne Davolio", "Andrew Fuller", "Janet White"});
            page.FindElement(By.Id("getRequired")).element.Click();
            page.FindElements(By.Id("requiredAttendes")).FindElements(By.TagName("tr")).elements.Count.ShouldBe(5);
            Reporter.AddStep("Set 5 elements in multi select", "5 elements selected", "Step 1");

            elementkedno.Clear();
            page.FindElement(By.Id("getRequired")).element.Click();
            page.FindElements(By.Id("requiredAttendes")).FindElements(By.TagName("tr")).elements.Count.ShouldBe(0);
            Reporter.AddStep("Clear all elements", "0 elements selected", "Step 2");
        }

        [Test]
        public void Given_FiveElementsSelected_When2Select_Then_2ElementsSelected()
        {
            page._driver.Url =
                "file:///C:/Users/pawy/source/repos/webautomation/WebAutomation/KendoMultiSelect.html";
            page._driver.Navigate();

            ICustomWebElement<KendoMultiSelect> elementkedno =
                page.FindElement(By.Id("required")).AsCustomElement<KendoMultiSelect>();
            elementkedno.SelectByText(new string[]
                {"Anne King", "Nancy King", "Anne Davolio", "Andrew Fuller", "Janet White"});
            page.FindElement(By.Id("getRequired")).element.Click();
            page.FindElements(By.Id("requiredAttendes")).FindElements(By.TagName("tr")).elements.Count.ShouldBe(5);
            Reporter.AddStep("Set 5 elements in multi select", "5 elements selected", "Step 1");

            elementkedno.SelectByText(new string[]
                {"Laura Peacock", "Andrew Callahan"});
            page.FindElement(By.Id("getRequired")).element.Click();
            page.FindElements(By.Id("requiredAttendes")).FindElements(By.TagName("tr")).elements.Count.ShouldBe(2);
            Reporter.AddStep("Set 2 elements", "2 elements selected", "Step 2");
        }
    }

}
