﻿using System.Threading;
using ArtOfTest.WebAii.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using Shouldly;
using WebAutomation.Selenium;
using Telerik.TestStudio.Core;
using Telerik.WebAii.Controls.Xaml.GridView;
using Telerik.WebAii.Controls;
using Telerik.WebAii.Controls.Xaml;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.TestTemplates;
using ArtOfTest.WebAii.TestAttributes;
using ArtOfTest.WebAii.Win32.Dialogs;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Silverlight.UI;
using ArtOfTest.WebAii.Silverlight;
using Telerik.TestingFramework.Controls.KendoUI;
using WebAutomation.Reporter;


namespace WebAutomaton.Tests
{
    [TestFixture]
    public class KendoTreeListTests : BaseSeleniumWrapperTest
    {
        [Test]
        public void Given_ElementNotVisible_When_Expanded_Then_ElementVisisble()
        {
            page._driver.Url =
                "file:///C:/Users/pawy/source/repos/webautomation/WebAutomation/KendoTreeList.html";
            page._driver.Navigate();

            page.FindElement(By.XPath("//*[contains(text(), 'Smith')]")).element.Displayed.ShouldBe(false);
            Reporter.AddStep("Find element in tree which is not main element", "Only main elements are displayed, sub elements are not displayed", "Step 1");

            ICustomWebElement<KendoTreeList> elementkedno =
            page.FindElement(By.Id("treelist")).AsCustomElement<KendoTreeList>();
            elementkedno.ExpandByPath(new string[]
                {"Employees", "Developers", "Smith"});
   
            page.FindElement(By.XPath("//*[contains(text(), 'Smith')]")).element.Displayed.ShouldBe(true);
            Reporter.AddStep("Expand groups to find last element", "tree List is expanded", "Step 2");

            
        }
    }
}
