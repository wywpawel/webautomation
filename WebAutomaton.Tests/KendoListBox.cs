﻿using System.Linq;
using System.Threading;
using ArtOfTest.WebAii.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using Shouldly;
using WebAutomation.Selenium;
using Telerik.TestStudio.Core;
using Telerik.WebAii.Controls.Xaml.GridView;
using Telerik.WebAii.Controls;
using Telerik.WebAii.Controls.Xaml;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.TestTemplates;
using ArtOfTest.WebAii.TestAttributes;
using ArtOfTest.WebAii.Win32.Dialogs;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Silverlight.UI;
using ArtOfTest.WebAii.Silverlight;
//using Telerik.TestStudio.Translators.Common;
using Telerik.TestingFramework.Controls.KendoUI;
using WebAutomation.Reporter;
using KendoListBox = WebAutomation.Selenium.KendoListBox;


namespace WebAutomaton.Tests
{
    [TestFixture]
    public class KendoListBoxtTests : BaseSeleniumWrapperTest
    {

        [Test]
        public void Given_FiveElementsSelected_WhenTwoCleared_Then_ThreeElementsSelected()
        {
            page._driver.Url =
                "file:///C:/Users/pawy/source/repos/webautomation/WebAutomation/KendoListBox.html";
            page._driver.Navigate();

            ICustomWebElement<KendoListBox> kendoListBox =
                page.FindElement(By.Id("avalaibleOptions")).AsCustomElement<KendoListBox>();
            kendoListBox.SelectByText(new string[]
                {"Nancy King", "Nancy Davolio", "Andrew Callahan", "Steven White", "Michael Suyama"});

            page.FindElement(By.Id("getSelected")).element.Click();
            page.FindElements(By.Id("selectedItems")).FindElements(By.TagName("tr")).elements.Count.ShouldBe(5);
            Reporter.AddStep("Set 5 elements in listBox", "5 elements selected", "Step 1");

            kendoListBox.ClearByText(new string[] {"Nancy King","Nancy Davolio"});
            page.FindElement(By.Id("getSelected")).element.Click();
            page.FindElements(By.Id("selectedItems")).FindElements(By.TagName("tr")).elements.Count.ShouldBe(3);
            Reporter.AddStep("Clear 2 elements", "3 elements selected", "Step 2");
        }

        [Test]
        public void Given_ThreeElementsSelected_WhenAllCleared_Then_NoneElementsSelected()
        {
            page._driver.Url =
                "file:///C:/Users/pawy/source/repos/webautomation/WebAutomation/KendoListBox.html";
            page._driver.Navigate();

            ICustomWebElement<KendoListBox> kendoListBox =
                page.FindElement(By.Id("avalaibleOptions")).AsCustomElement<KendoListBox>();
            kendoListBox.SelectByText(new string[]
                {"Nancy King", "Nancy Davolio", "Andrew Callahan"});

            page.FindElement(By.Id("getSelected")).element.Click();
            page.FindElements(By.Id("selectedItems")).FindElements(By.TagName("tr")).elements.Count.ShouldBe(3);
            Reporter.AddStep("Set 3 elements in listBox", "3 elements selected", "Step 1");

            kendoListBox.Clear();
            page.FindElement(By.Id("getSelected")).element.Click();
            page.FindElements(By.Id("selectedItems")).FindElements(By.TagName("tr")).elements.Count.ShouldBe(0);
            Reporter.AddStep("Clear all elements", "0 elements selected", "Step 2");
        }

        [Test]
        public void Given_NoneElementsSelected_WhenAllSelected_Then_AllElementsSelected()
        {
            page._driver.Url =
                "file:///C:/Users/pawy/source/repos/webautomation/WebAutomation/KendoListBox.html";
            page._driver.Navigate();

            ICustomWebElement<KendoListBox> kendoListBox =
                page.FindElement(By.Id("avalaibleOptions")).AsCustomElement<KendoListBox>();
            kendoListBox.SelectByText(new string[]
                {});

            page.FindElement(By.Id("getSelected")).element.Click();
            page.FindElements(By.Id("selectedItems")).FindElements(By.TagName("tr")).elements.Count.ShouldBe(0);
            Reporter.AddStep("Set 0 elements in listBox", "0 elements selected", "Step 1");

            kendoListBox.SelectByText(kendoListBox.GetItems().Select(x => x.Text).ToArray());
            page.FindElement(By.Id("getSelected")).element.Click();
            page.FindElements(By.Id("selectedItems")).FindElements(By.TagName("tr")).elements.Count.ShouldBe(7);
            Reporter.AddStep("Select all elements", "7 elements selected", "Step 2");
        }
    }

}
