﻿using System;
using System.IO;
using System.Linq;
using ArtOfTest.WebAii.Javascript;
using Newtonsoft.Json;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using WebAutomation.Page;
using WebAutomation.Reporter;

namespace WebAutomaton.Tests
{
    public class BaseSeleniumWrapperTest : System.IDisposable
    {
        [SetUp]
        public void SetUp()
        {
            Reporter.results[0].tests.Add(new Test());
            Reporter.currentTestIndex = Reporter.results[0].tests.Count - 1;
            Reporter.testStartTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        }

        [TearDown]
        public void TearDown()
        {
            Reporter.results[0].tests[Reporter.currentTestIndex].context += "]";
            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
            {
                Reporter.UpdateTest(false);
            }
            else
            {
                Reporter.UpdateTest(true);
            }

            Reporter.results[0].duration += Reporter.results[0].tests[currentTestIndex].duration;
        }

        
        
        public int currentTestIndex { get; set; }
        public Page page { get; set; }
        public Reporter Reporter { get; set; }
        public long suiteStartTime { get; set; }
        public BaseSeleniumWrapperTest()
        {
            page = new Page(new FirefoxDriver());
            suiteStartTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            Reporter =new Reporter(page);
            Directory.CreateDirectory("./screenshots/" + TestContext.CurrentContext.Test.Name);
            DirectoryInfo di = new DirectoryInfo("./screenshots/"+ TestContext.CurrentContext.Test.Name);
            foreach (FileInfo file in di.EnumerateFiles())
            {
                file.Delete();
            }
        }
      

        public void Dispose()
        {
            page._driver.Quit();
            
            Reporter.results[0].duration = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - suiteStartTime;
            Reporter.stats.end = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            Reporter.stats.duration = Reporter.results[0].duration;

            string jsonResult = JsonConvert.SerializeObject(Reporter);
            string path = string.Format("./jsonResult{0}.json", TestContext.CurrentContext.Test.Name);
            if (File.Exists(path))
                File.Delete(path);

            using (var tw = new StreamWriter(path, true))
            {
                tw.WriteLine(jsonResult);
                tw.Close();
            }
        }
    }
}