﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;

namespace WebAutomation.Reporter
{
    public class Reporter
    {
        [JsonIgnore]
        public int currentTestIndex { get; set; }
        [JsonIgnore]
        public Page.Page page { get; }

        [JsonIgnore]
        public long testStartTime { get; set; }
        public Stats stats { get; set; }
        public IList<Result> results { get; set; }

        public Reporter(Page.Page page)
        {
            stats = new Stats();
            results = new List<Result>
            {
                new Result()
            };
            stats.start = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

            this.page = page;
        }

        [JsonIgnore]
        public Test CurrentTest
        {
            get => results[0].tests[currentTestIndex];
        }

     public void AddStep(string stepInstruction, string expectedResult, string imgStepName)
        {
            CurrentTest.context += CurrentTest.context.Length > 1 ?  $",\"{stepInstruction}\"" : $"\"{stepInstruction}\"";
            CurrentTest.context += $",\"{expectedResult}\"";
            //Screenshot ss = ((ITakesScreenshot)page._driver).GetScreenshot();
            
            CurrentTest.context += $",\"{TakeScreenshot(TestContext.CurrentContext.Test.Name + " - " +imgStepName)}\"";
        }

        public string TakeScreenshot(string imgName)
        {

            Screenshot ss = ((ITakesScreenshot)page._driver).GetScreenshot();

            string filepath = "./screenshots/" + TestContext.CurrentContext.Test.Name;

            if (!Directory.Exists(filepath))
                Directory.CreateDirectory(filepath);

            ss.SaveAsFile(filepath + "/" + imgName + ".png", ScreenshotImageFormat.Png);
            return (filepath + "/" + imgName + ".png");

        }

        public void UpdateTest(bool testPassed)
        {
            results[0].tests[currentTestIndex].duration = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - testStartTime;
            
            if (testPassed)
            {
                stats.passes += 1;
                results[0].tests[currentTestIndex].pass = true;
                results[0].tests[currentTestIndex].fail = false;
                results[0].tests[currentTestIndex].skipped = false;
            }
            else
            {
                stats.failures += 1; 
                results[0].tests[currentTestIndex].pass = false;
                results[0].tests[currentTestIndex].fail = true;
                results[0].tests[currentTestIndex].skipped = false;
            }
        }
    }

    public class Result
    {
        public string uuid { get; set; }
        public string title { get; set; }
        public string fullFile { get; set; }
        public string file { get; set; }
        public IList<string> beforeHooks { get; set; }
        public IList<string> afterHooks { get; set; }
        public IList<Test> tests { get; set; }
        public IList<string> suites { get; set; }
        public IList<string> passes { get; set; }
        public IList<string> failures { get; set; }
        public IList<string> pending { get; set; }
        public IList<string> skipped { get; set; }
        public long duration { get; set; }
        public bool root { get; set; }
        public int _timeout { get; set; }



        public Result()
        {
            title = TestContext.CurrentContext.Test.ClassName;
            fullFile = "";
            file = TestContext.CurrentContext.Test.ClassName;
            uuid = System.Guid.NewGuid().ToString();
            beforeHooks = new List<string>();
            afterHooks = new List<string>();
            tests = new List<Test>();
            suites = new List<string>();
            passes = new List<string>();
            failures = new List<string>();
            pending = new List<string>();
            skipped = new List<string>();
        }
    }

    public class Test
    {
        public string title { get; set; }
        public string fullTitle { get; set; }
        public long duration { get; set; }
        public bool pass { get; set; }
        public bool fail { get; set; }

        public bool pending { get; set; }
        public string context { get; set; }
        public string code { get; set; }
        public Err err { get; set; }
        public string uuid { get; set; }
        public bool isHook { get; set; }
        public bool skipped { get; set; }

        public Test()
        {
            title = TestContext.CurrentContext.Test.Name;
            fullTitle = "";
            code = "";
            uuid = System.Guid.NewGuid().ToString();
            context = "[";
            err =new Err();
        }
    }

    public class Err
    {
    }

    public class Stats
    {
        public int suites { get; set; }
        public int tests { get; set; }
        public int passes { get; set; }        
        public int pending { get; set; }        
        public int failures { get; set; }        
        public string start { get; set; }        
        public string end { get; set; }        
        public long duration { get; set; }        
        public int testsRegistered { get; set; }        
        public int passPercent { get; set; }        
        public int pendingPercent { get; set; }
        public int other { get; set; }
        public bool hasOther { get; set; }
        public int skipped { get; set; }
        public bool hasSkipped { get; set; }

        public Stats()
        {
            suites = 1;
        }
    }


}
