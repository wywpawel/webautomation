﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace WebAutomation.Selenium
{
    public interface ICustomWebElement<out TElement>
    {
        TElement Element { get; }
    }

    public class CustomWebElement<TElement> : ICustomWebElement<TElement>
    {         

        public CustomWebElement(TElement element)
        {
            Element = element;
        }
        public TElement Element { get;}
    }
}
