﻿using OpenQA.Selenium;
using WebAutomation.Page;
using WebAutomation.Selenium.CustomElementInterfaces;

namespace WebAutomation.Selenium
{
    public abstract class BaseWebElement : IBaseElement
    {
        public IWebElement _element;
        public IWebDriver _driver;
        public BaseWebElement(WebContext context)
        {
            _element = context.element;
            _driver = context.driver;
        }

        bool IBaseElement.IsDisplayed()
        {
            return _element.Displayed;
        }
    }
}