﻿namespace WebAutomation.Selenium
{
    public class TextValue
    {
        public string Text { get; set; }
        public string Value { get; set; }

        public TextValue(string text, string value)
        {
            Text = text;
            Value = value;
        }
    }
}