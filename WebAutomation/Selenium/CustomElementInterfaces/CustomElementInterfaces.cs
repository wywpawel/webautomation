﻿using System.Collections.Generic;

namespace WebAutomation.Selenium.CustomElementInterfaces
{

    public interface IClearable
    {
        void Clear();
    }

    public interface ICustomSelect
    {
        void SelectByText(string[] text, bool clearElement);
    }

    public interface IBaseElement
    {
        bool IsDisplayed();
    }

    public interface IMultipleItems
    {
        IList<TextValue> GetItems();
        IList<string> GetSelectedItems();
    }

    public interface IListBox
    {
        void ClearByText(string[] text);

    }

    public interface ITreeList
    {
        void ExpandByPath(string[] pathList);
    }
}
