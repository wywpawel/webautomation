﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using WebAutomation.Page;
using WebAutomation.Selenium.CustomElementInterfaces;

namespace WebAutomation.Selenium
{
    public class KendoMultiSelect : BaseWebElement, IClearable, ICustomSelect, IMultipleItems
    {
        private readonly string _id;

        public KendoMultiSelect(WebContext context) : base(context)
        {
            _id = _element.GetAttribute("id");
        }

        public void Clear()
        {
            var script = $"kms.value([])";
            RunJavaScript(script);
        }

        public IList<TextValue> GetItems()
        {
            var script = $"return (kms.dataSource.data().map(function(source){{return{{Value: source.value, Text: source.text}};}}))";
            
            return RunJavaScript(script).AsTextValues();
        }

        public IList<string> GetSelectedItems()
        {
            var script = $"return (kms.value());";
            return RunJavaScript(script).AsStringValues();
        }
       
        public void SelectByText(string[] textValues, bool clearElement)
        {
            List<string> dropDownValues = new List<string>();
            foreach (var textValue in textValues)
            {
                dropDownValues.Add($"'{textValue}'");
            }

            string valuesToSelect;
            if (clearElement == false)
            {
                var scriptValues = $"return kms.value();";
                var selectedValues = RunJavaScript(scriptValues).AsStringValues();
                valuesToSelect = string.Join(",", selectedValues.Union(dropDownValues));

            }
            else
            {
                valuesToSelect = string.Join(",", dropDownValues);
            }

            var script = $"kms.value([{valuesToSelect}])";
            RunJavaScript(script);
        }

        private object RunJavaScript(string script)
        {
            var scriptToRun = $@"var kms = $('#{_id}').data('kendoMultiSelect'); {script}";
            return ((IJavaScriptExecutor)_driver).ExecuteScript(scriptToRun);
        }
    }
}
