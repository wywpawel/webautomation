﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using WebAutomation.Page;
using WebAutomation.Selenium.CustomElementInterfaces;

namespace WebAutomation.Selenium
{
    public class KendoTreeList : BaseWebElement, ITreeList
    {
        private readonly string _id;

        public KendoTreeList(WebContext context) : base(context)
        {
            _id = _element.GetAttribute("id");

        }

        public void ExpandByPath(string[] path)
        {

            List<string> pathList = path.ToList();
            pathList.RemoveRange(0, 1);

            string scriptToRun = "";
                //$"ktl.expand(ktl.content.find(\"tr[data-uid=\"+ktl.dataSource.get(ktl.dataSource.options.data.find(item => item.name === \"{firstParentElement}\").id).uid + \"]\"))";
            string scriptToExpand = "";

            for (int i = 0; i < path.Length; i++)
            {
                if (i == 0)
                {
                    scriptToRun += $"ktl.expand(ktl.content.find(\"tr[data-uid=\"+ktl.dataSource.get(ktl.dataSource.options.data.find(item => item.name === \"{path[0]}\").id).uid + \"]\"));";

                }
                else
                {
                    scriptToRun += $"var parentElement = ktl.dataSource.options.data.find(item => item.name === \"{path[i-1]}\") ;ktl.expand(ktl.content.find(\"tr[data-uid=\"+ktl.dataSource.get(ktl.dataSource.options.data.find(item => item.name === \"{path[i]}\" && item.parentId === parentElement.id).id).uid + \"]\"));";

                }
            }
            foreach (var textValue in path)

            {
                scriptToExpand +=
                    $"ktl.expand(ktl.content.find(\"tr[data-uid=\"+ktl.dataSource.get(ktl.dataSource.options.data.find(item => item.name === \"{textValue}\").id).uid + \"]\"));";
            }
          
            RunJavaScript(scriptToRun);
        }

        private object RunJavaScript(string script)
        {
            var scriptToRun = $@"var ktl = $('#{_id}').data('kendoTreeList'); {script}";
            return ((IJavaScriptExecutor)_driver).ExecuteScript(scriptToRun);
        }
    }
}
    