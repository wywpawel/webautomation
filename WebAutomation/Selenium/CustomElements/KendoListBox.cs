﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using WebAutomation.Page;
using WebAutomation.Selenium.CustomElementInterfaces;

namespace WebAutomation.Selenium
{
    public class KendoListBox : BaseWebElement, IClearable, ICustomSelect, IMultipleItems, IListBox
    {
        private readonly string _id;
        private readonly string _connectedWithId;

        public KendoListBox(WebContext context) : base(context)
        {
            _id = _element.GetAttribute("id");
            _connectedWithId = RunJavaScript("return klb.toolbar.listBox.options.connectWith", _id).ToString();
        }

        public void ClearByText(string[] textValues)
        {
            List<string> dropDownValues = new List<string>();
            string scriptToClear = "";
            foreach (var textValue in textValues)
            {
                scriptToClear += $"klb.select(klb.items().toArray().find((item) => (item.textContent === \"{textValue}\"))); klb._executeCommand(\"transferTo\");";
            }

            //var scriptToClear = $"klb.items().toArray().forEach((item) => {{klb.select(item); klb._executeCommand(\"transferTo\");}});";

            RunJavaScript(scriptToClear, _connectedWithId);
        }

        public void Clear()
        {
            var aa11 = GetItems();
            var aa12 = GetSelectedItems();

            var scriptToClear = $"klb._executeCommand(\"transferAllFrom\");";

            RunJavaScript(scriptToClear, _id);
            var aa21 = GetItems();
            var aa22 = GetSelectedItems();
        }

        public IList<TextValue> GetItems()
        {
            var script = $"return (klb.dataSource.data().map(function(source) {{ return {{ Value: source.value, Text: source.text}};}}));";
            //var script = $"return (kms.dataSource.data().map(function(source){{return{{Value: source.value, Text: source.text}};}}))";

            return RunJavaScript(script, _id).AsTextValues();
        }

        public IList<string> GetSelectedItems()
        {
            var script = $"return (klb.items().toArray().map(function(source){{return  source.textContent;}}));";
            //var testpurpose = RunJavaScript(script, _connectedWithId).AsTextValues();
            return RunJavaScript(script, _connectedWithId).AsStringValues();
        }
       
        public void SelectByText(string[] textValues, bool clearElement)
        {
            List<string> dropDownValues = new List<string>();
            string scriptToSelect = "";
            foreach (var textValue in textValues)
            {
                dropDownValues.Add($"'{textValue}'");
                scriptToSelect += $"klb.select(klb.items().toArray().find((item) => (item.textContent === \"{textValue}\"))); klb._executeCommand(\"transferTo\");";
            }
            //var scriptToSelect = $"klb.select(klb.items.find((item) => return item.textContent === {textValue}))";
            //string valuesToSelect;
            //if (clearElement == false)
            //{
            //    var scriptValues = $"return kms.value();";
            //    var selectedValues = RunJavaScript(scriptValues).AsStringValues();
            //    valuesToSelect = string.Join(",", selectedValues.Union(dropDownValues));

            //}
            //else
            //{
            //    valuesToSelect = string.Join(",", dropDownValues);
            //}

            //var script = $"kms.value([{valuesToSelect}])";
            RunJavaScript(scriptToSelect, _id);
        }

        private object RunJavaScript(string script, string listBoxId)
        {
            var scriptToRun = $@"var klb = $('#{listBoxId}').data('kendoListBox'); {script}";
            return ((IJavaScriptExecutor)_driver).ExecuteScript(scriptToRun);
        }
    }
}
    