﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using WebAutomation.Page;
using WebAutomation.Selenium.CustomElementInterfaces;

namespace WebAutomation.Selenium

{
    public static class CustomElementsExtension
    {
        #region Select

        public static ICustomWebElement<TElement> AsCustomElement<TElement>(this WebContext webContext)
        {
            return new CustomWebElement<TElement>((TElement)Activator.CreateInstance(typeof(TElement), webContext));
        }        

        #endregion

        #region Interactions

        public static void Clear(this ICustomWebElement<IClearable> context)
        {
            context.Element.Clear();
        }

        public static void SelectByText(this ICustomWebElement<ICustomSelect> context, string[] textValues, bool clearElement = true )
        {
            context.Element.SelectByText(textValues, clearElement);
        }
        public static void ClearByText(this ICustomWebElement<IListBox> context, string[] textValues)
        {
            context.Element.ClearByText(textValues);
        }
        public static bool IsDisplayed(this ICustomWebElement<IBaseElement> context)
        {
            return context.Element.IsDisplayed();
        }

        public static IList<TextValue> GetItems(this ICustomWebElement<IMultipleItems> context)
        {
            return context.Element.GetItems();
        }

        public static IList<string> GetSelectedItems(this ICustomWebElement<IMultipleItems> context)
        {
            return context.Element.GetSelectedItems();
        }

        public static void ExpandByPath(this ICustomWebElement<ITreeList> context, string[] pathList)
        {
            context.Element.ExpandByPath(pathList);
        }

        #endregion

    }
}