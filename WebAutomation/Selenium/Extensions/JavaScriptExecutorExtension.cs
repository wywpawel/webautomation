﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebAutomation.Selenium
{
    public static class JavaScriptExecutorExtension
    {
        public static IList<TextValue> AsTextValues(this object objectValue)
        {
            if (objectValue == null)
                return null;

            var result = new List<TextValue>();
            dynamic resultArray = objectValue;
            foreach (var item in resultArray)
            {
                var propertiesDict = item as Dictionary<string, object>;
                if (propertiesDict != null)
                {
                    var text = propertiesDict.ContainsKey("Text") ? propertiesDict["Text"]?.ToString() : null;
                    var value = propertiesDict.ContainsKey("Value") ? propertiesDict["Value"]?.ToString() : null;

                    result.Add(new TextValue(text, value));
                }
            }

            return result;
        }
        public static IList<string> AsStringValues(this object objectValue)
        {
            if (objectValue == null)
                return null;

            var result = new List<string>();
            dynamic resultArray = objectValue;
            foreach (var item in resultArray)
            {
                result.Add($"'{item}'");
            }

            return result;
        }
    }
}
