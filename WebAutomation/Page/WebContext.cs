﻿using System.Collections.ObjectModel;
using OpenQA.Selenium;

namespace WebAutomation.Page
{
    public class WebContext
    {
        public IWebElement element { get; set; }
        public ReadOnlyCollection<IWebElement> elements { get; set; }
        public IWebDriver driver { get; set; }
        public WebContext(IWebDriver driver)
        {
            this.driver = driver;
        }

        public WebContext(IWebDriver driver, ReadOnlyCollection<IWebElement> elements)
        {
            this.driver = driver;
            this.elements = elements;
        }

        public WebContext FindElement(By by)
        {
            this.element = driver.FindElement(by);
            return this;
        }

        public WebContext FindElements(By by)
        {
            this.elements = driver.FindElements(by);
            return this;
        }
    }
        

}
