﻿using System;
using System.Collections.ObjectModel;
using OpenQA.Selenium;

namespace WebAutomation.Page
{
    public class Page
    {
        public WebContext context{get;set;}
        public IWebDriver _driver { get; set; }
       
        public Page(IWebDriver driver)
        {
            _driver = driver;
        }

        public WebContext FindElement(By by)
        {
            context = new WebContext(_driver);
            return context.FindElement(by);
        }        

        public WebContext FindElements(By by)
        {
            context = new WebContext(_driver);
            return context.FindElements(by);
        }
    }
}
